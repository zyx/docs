---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Zeke的知识库"
  text: "A VitePress Site"
  tagline: More than you can see.
  image:
    src: /background.svg
    alt: 背景图
  actions:
    - theme: brand
      text: Markdown 语法
      link: /wiki-examples
    - theme: alt
      text: Vitepress 搭建与部署
      link: /learn/soft/vitepress

features:
  - title: 学习记录 
    details:
    link: /learn/first  
  - title: 工具使用 
    details:
    link: /tool/first 
  - title: 项目经历 
    link: /projects/first 
    details: 
  - title: 服务器 
    link: /server/first 
    details: 
---

