import { defineConfig } from 'vitepress'
import { set_sidebar } from "../utils/auto-gen-sidebar.mjs";	

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base: '/',
  title: "Zyke的文档网站",
  description: "A VitePress Site",
  head: [["link", { rel: "icon", href: "/head.svg" }]],
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    // 配置logo位置，public目录
    // sidebar: false, // 关闭侧边栏
    aside: "left", // 设置右侧侧边栏在左侧显示
    outlineTitle:"文章目录",
    outline:[2,6],
    logo: "monitor.svg", 
    //页面右上角菜单栏
    nav: [  
      { text: '首页', link: '/' },
      { text: '学习记录',
        items: [
          {
            text: 'FreeCodeCamp',
            link: '/learn/freecodecamp/index',
          },
          {
            text: '一生一芯',
            link: '/learn/ysyx',
          },
        ],
      },
      { text: '工具使用', link: '/tool/first'},
      { text: '服务器', link: '/server/first'},
      { text: '关于我', link: '/me/about' }
    ],
    //侧边栏
    // sidebar: [
    //   {
    //     text: '基础知识',
    //     items: [
    //       { text: 'Markdown Examples', link: '/markdown-examples' },
    //       { text: 'Runtime API Examples', link: '/api-examples' },
    //     ],
    //   },
    //   {
    //     text: '工具学习',
    //     items: [
    //       { text: 'Docker', link: '/learn/docker' },
    //       { text: 'Runtime API Examples', link: '/api-examples' },
    //     ],
    //   },
    //   {
    //     text: '项目复刻',
    //     items: [
    //       { text: 'Markdown Examples', link: '/markdown-examples' },
    //       { text: 'Runtime API Examples', link: '/api-examples' },
    //     ],
    //   },
    // ],
    //页面右上角跳转图标
    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' },
    ]
  }
})



