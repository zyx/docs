# 接口测试工具

## 核心

method.apply(cls, args)

执行method()并传入对象cls和参数args

apply 方法

```JS
function.apply(thisArg, argsArray)
```

- thisArg：当执行函数时，用作 this 值的对象。
- argsArray：一个数组或类数组对象，其中的数组元素将作为参数传递给函数。



## 代码

```JS

<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>接口工具</title>
		<style>
			body {
				font-family: Arial, sans-serif;
				padding: 20px;
			}
		</style>
		<script>
			// 全局对象eda
			var eda = {};

			function invokeMethod() {
				let className = document.getElementById('className').value;
				let methodName = document.getElementById('methodName').value;
				let params = document.getElementById('params').value;

				try {
					// 获取类和方法
					let cls = eda[className];
					let method = cls ? cls[methodName] : null;

					if (method && typeof method === 'function') {
						// 将参数字符串转换为实际参数数组
						let args = params ? JSON.parse(params) : [];
						// 调用方法
						let result = method.apply(cls, args);

						// 处理异步函数
						if (typeof result?.then === 'function') {
							result
								.then(function (value) {
									document.getElementById('output').textContent = '[异步函数]返回值: ' + value + ' | 类型: ' + typeof value;
								})
								.catch(function (error) {
									document.getElementById('output').textContent = 'Error: ' + error.message;
								});
						} else {

							document.getElementById('output').textContent = '返回值: ' + result + ' | 类型: ' + typeof result;
						}
					} else {
						document.getElementById('output').textContent = '方法调用有误';
					}
				} catch (e) {
					document.getElementById('output').textContent = 'Error: ' + e.message;
				}
			}
		</script>
	</head>
	<body>
		<h2>接口测试工具</h2>
		<label for="className">类名:</label>
		<input type="text" id="className" placeholder="Class name" />
		<br /><br />
		<label for="methodName">方法名:</label>
		<input type="text" id="methodName" placeholder="Method name" />
		<br /><br />
		<label for="params">参数 (JSON):</label>
		<input type="text" id="params" placeholder='["param1", "param2"]' />
		<br /><br />
		<button onclick="invokeMethod()">调用</button>
		<h3>输出:</h3>
		<div id="output"></div>
	</body>
</html>

```