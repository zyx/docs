# CH32V003 开源掌机

CH32V003系列是基于青稞RISC-V2A内核设计的通用微控制器，支持48MHz系统主频，具有宽压、单线调试、低功耗、超小封装等特点。

## 编译下载

可采用 [MounRiver](http://mounriver.com/download) 进行开发。

下载需要使用到WCH-LinkE，引脚连接VCC、GND、SWD(PD1)。

首先使用 [Zadio](https://zadig.akeo.ie/#google_vignette) 安装 WinUSB 驱动程序。

在 WCH-LinkUtility 中选择Hex文件，确定芯片信息，确定是否处于读保护(Read Protect)后就可以进行下载了。

![alt text](image.png)

:::warning 注意
下载器蓝色灯亮起代表处于 ARM 模式，必须先切换到 RISC-V 模式,可以在 WCH-LinkUtility 中选择`WCH-LinkRV`再点击`Set`即可切换。
:::

## PlatformIO

参考链接：

https://github.com/AlexanderMandera/arduino-wch32v003

[CH32V003-GameConsole](https://github.com/wagiminator/CH32V003-GameConsole?tab=readme-ov-file)

[WCH-LinkUtility](https://www.wch.cn/downloads/WCH-LinkUtility_ZIP.html)

[沁恒CH32V003(一): CH32V003F4P6开发板上手报告和Win10环境配置](https://www.cnblogs.com/milton/p/16838010.html)