# CSS样式

## CSS选择器

Class 一组元素，ID 特定元素

类型选择器

```css
/*选择所有的<p>元素 */
p {
    color: blue;
}
```

类选择器

```css
/*选择所有class="highlight"的元素 */
.highlight {
    background-color: yellow;
}
```

ID选择器

```css
/*选择所有id="header"的元素 */
#header {
    font-size: 24px;
}
```

属性选择器

```css
/*选择具有属性_blank的元素 */
a[target="_blank"] {
    color: green;
}
```

伪类选择器

```css
/*选择处于hover(悬停)状态的元素 */
a:hover {
    color: red;
}
```

伪元素选择器

```css
/*元素的特定部分，p元素的第一行 */
p::first-line {
    font-weight: bold;
}
```

组合器...

## 参数输入输出

