# 学习记录

## 系统性

[FreeCodeCamp](/learn/freecodecamp/index)

[JStraining](/learn/jstraining/index)

## 前端

[CSS样式](/learn/front/css)

[网页](/learn/soft/web)

## 硬件

[OrangePi Zero3 H618](/learn/hardware/zero3)

[Altium Designer 24](/learn/hardware/altium-designer)

## 软件

[Vitepress的搭建与部署](/learn/soft/vitepress)

[Python学习记录](/learn/soft/python)

[AxureRP8的基础使用](/learn/soft/axure)

[Datasheet解析](/learn/soft/datasheet-analyzing)

[Gradio学习记录](/learn/soft/gradio)

[Learn C the hard way](/learn/soft/the-hard-way-learn-c)

[Git 管理代码文件](/learn/soft/git)

## 嵌入式

[Linux101](/learn/inset/linux101)

[计算机教育中缺失的一课](/learn/inset/missing-semester-cs)

[Verilog学习记录](/learn/inset/verilog)

## 一生一芯

[一生一芯学习记录](/learn/ysyx)

[南京大学计算机系统基础 课程实验PA](/learn/pa)
