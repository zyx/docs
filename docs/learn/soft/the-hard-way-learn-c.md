## Learn C the hard way

不草率写代码

学习时不使用IDE

### 环境安装

```Shell
$ sudo apt-get install build-essential
```

### 文本编辑器 `Vim`

Sublime Text

Do not try to memorize,learn by usage.

**移动光标**

- ` ↑ `  k  
- ` ↓ `  j 
- ` ← `  h
- ` → `  l

## 指令

- `删除`  x
- `撤回`  u
- `撤回撤回` `Ctrl + r`
- % 括号配对
- o 插入一行
- 0 回到开头
- v 可视化模式
- y 复制
- p 粘贴
  
操作符 + 动作列表

- d - `删除`操作符
- c - `更改`操作符
- r - `替换`操作符
- g - `跳转`操作符
- `Ctrl + g` 文件名 光标位置
- `gg` 跳转到`文件开头`
- G 文件`结尾`
- 数字 ＋ G 跳到到数字行
- / + 查找字符
- n 查找下一个 N 查找上一个

- w - 到下一个`单词开头`
- e - 到`单词末尾`
- $ - 到`行末`


