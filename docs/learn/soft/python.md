# Python

[A Byte Of Python 中文版](https://github.com/LenKiMo/byte-of-python)

[笨办法学Python PDF](../../public/笨办法学Python.pdf)

[Python 库索引](https://pypi.org/)

明了胜过晦涩，显式优于隐式。

format

文档字符串  

该文档字符串所约定的是一串多行字符串，其中第一行以某一大写字母开始，以句号结束。第二行为空行，后跟的第三行开始是任何详细的解释说明。在此_强烈建议_你在你所有重要功能的所有文档字符串中都遵循这一约定。

__doc__
函数 重用程序
模块 重用函数
sys.argv 变量
模块的 "__name__"
from...import
from mymodule import *
dir 函数
包

- world/
    - __init__.py
    - asia/
        - __init__.py
        - india/
            - __init__.py
            - foo.py
    - africa/
        - __init__.py
        - madagascar/
            - __init__.py
            - bar.py

数据结构 列表(List)、元组(Tuple)、字典(Dictionary)和集合(Set)
储一系列相关数据的集合。
列表 有序项目的集合 序列` [, , ]`可变
类 带有方法 为该类定制的函数
类 带有字段(Field) 为该类定制的属性
元组 不可变 `(, , )` 安全的采用一组数值
字典 地址薄 键值(Keys)(即姓名)与_值(Values)(即地址等详细信息)联立到一起 dict
只能使用简单对象作为键值(不可变的对象),可以使用可变或不可变的对象作为字典中的值

d = `{key : value1 , key2 : value2}`

items方法

序列 资格测试(Membership Test) 索引操作(Indexing Operations)

集合 更关注元素的“存在性”而不是它们的“顺序”或“重复性”时，就会选择使用集合

引用 变量和对象之间赋值 变量引用了内存中的某个位置 对于复杂对象，需要使用特定的方法（如切片操作）来创建真正的副本，以避免无意中修改原始数据 列表复制不会创建副本

## 面向对象

类

实例化

属性--类中变量

类属性

实例属性

类的方法

第一个参数self

示例后能使用

__init__() 方法 初始化参数，实例化时需提供



