# 前端

[菜鸟教程](https://www.runoob.com/)

[W3School](https://www.w3school.com.cn/index.html)

页面逻辑：从左往右，从上到下，从外到内。

## HTML

元素

属性

标签

## CSS

`margin-top`不生效加`padding-top`和`box-sizing:border-box;`

### 导入方式

内联样式 head标签内 `<style>`

内部样式 `<h2 style="color:rgb(90, 108, 108);">二级标题</h2>`

外部样式 `<link>`

### 选择器

用于针对特定元素或一组元素定义样式

元素选择器

类选择器 .类名 `<h3 class="类">...</h3>`

ID选择器 #ID `<h3 id="ID">...</h3>`

通用选择器 *

子元素选择器 .father > .son 

```html
<dir class = "father">
    <p class = "son">子元素</p>
</dir>
```

后代选择器

兄弟选择器

伪类选择器

### 常用属性

## JavaScript

### 输入

document.getElementById("demo")

### 逻辑

变量 let

常量 const 硬编码大写形式

数据类型 Number,String(反引号可用${}),Boolean,Null(值未知),Undefined(未赋值),Object

运算符 `typeof` `in`

函数 表示一个“行为”的值

箭头函数 `let func = (arg1, arg2, ..., argN) => expression;` 左侧获取参数,右侧表达式

传递函数本身，不用箭头函数则会立即执行并传递函数结果

```JS
function ask(question, yes, no) {
  if (confirm(question)) yes();
  else no();
}

ask(
  "Do you agree?",
  () => alert("You agreed."),
  () => alert("You canceled the execution.")
);
```

代码风格


注释风格 JSDoc：用法、参数和返回值

```JS
/**
 * 返回 x 的 n 次幂的值。
 *
 * @param {number} x 要改变的值。
 * @param {number} n 幂数，必须是一个自然数。
 * @return {number} x 的 n 次幂的值。
 */
function pow(x, n) {
  ...
}
```

代码测试BDD 测试、文档、示例

对象 属性 键值对 方括号 属性名字符串 点符号 ＋ 属性名

![图 4](/storage/images/docs/learn/soft/front/front_20241122_115816.png)  

计算属性

简写

所以，大部分时间里，当属性名是已知且简单的时候，就使用点符号。如果我们需要一些更复杂的内容，那么就用方括号。

遍历一个对象的所有键 "for..in" 循环

### 输出

document.write()

innerHTML

console.log()

alert()

prompt()

confirm()


## 网络请求

[请求方式]

获取 `GET` 请求服务器上的特定资源

发送 `POST` 向服务器提交数据，可上传表单和文件

[HTTP请求头] 

提供了关于请求或响应的附加信息

![图 5](/storage/images/docs/learn/soft/front/front_20241204_142454.png)  

[请求体]

客户端发送给服务器的数据

请求体可以包含多种类型的数据，如表单数据、JSON、XML等。

可用 `JSON.stringify()` 将数据转换为 JSON 字符串。

[响应解析]

服务器接收到客户端请求后的回应，内容可能是JSON、HTML、XML或任何其他格式的数据。

检查状态码,读取响应体,解析内容

可用 `JSON.parse()` 将 JSON 字符串转换为 JavaScript 对象。

### 请求示例

[Fetch-API]

```js
fetch('https://example.com/api', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer your-token'
    },
    body: JSON.stringify({ name: 'John', age: 30 })
})
.then(response => response.json())
.then(data => console.log(data))
.catch(error => console.error('Error:', error));
```

[SYS_ClientUrl.request()]

![图 6](/storage/images/docs/learn/soft/front/front_20241204_144940.png)  

```js
// 异步函数用于发送请求
async function sendRequest() {
  try {
    // 定义请求参数
    const url = 'https://example.com/api/data';
    const method = 'POST';
    const data = JSON.stringify({ key: 'value' });
    const headers = { 'Content-Type': 'application/json' };

    // 发起请求
    const response = await eda.sys_ClientUrl.request(url, method, data, { headers });

    // 处理响应
    console.log(response);
  } catch (error) {
    console.error('Request failed:', error);
  }
}
```

[imgbb图床调用示例]

```shell
curl --location --request POST "https://api.imgbb.com/1/upload?expiration=600&key=YOUR_CLIENT_API_KEY" --form "image=R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
```

Fetch API 

```javascript
const apiKey = 'YOUR_CLIENT_API_KEY';
const imageBase64 = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
const formData = new FormData();
formData.append('image', imageBase64);

fetch(`https://api.imgbb.com/1/upload?expiration=600&key=${apiKey}`, {
  method: 'POST',
  body: formData
})
.then(response => response.json()) // 解析 JSON 响应
.then(data => {
  console.log('Image uploaded successfully:', data);
})
.catch(error => {
  console.error('Error uploading image:', error);
});
```

异步函数

```javascript
const apiKey = 'YOUR_CLIENT_API_KEY';
const imageBase64 = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
const formData = new FormData();
formData.append('image', imageBase64);

async function uploadImage() {
  try {
    const response = await fetch(`https://api.imgbb.com/1/upload?expiration=600&key=${apiKey}`, {
      method: 'POST',
      body: formData
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    console.log('Image uploaded successfully:', data);
  } catch (error) {
    console.error('Error uploading image:', error);
  }
}

```

[阿里云涂鸦作画API调用示例]

```shell
curl --location 'https://dashscope.aliyuncs.com/api/v1/services/aigc/image2image/image-synthesis' \
--header 'X-DashScope-Async: enable' \
--header "Authorization: Bearer $DASHSCOPE_API_KEY" \
--header 'Content-Type: application/json' \
--data '{
    "model": "wanx-sketch-to-image-lite",
    "input": {
        "sketch_image_url": "https://huarong123.oss-cn-hangzhou.aliyuncs.com/image/%E6%B6%82%E9%B8%A6%E8%8D%89%E5%9B%BE.png",
        "prompt": "一棵参天大树"
    },
    "parameters": {
        "size": "768*768",
        "n": 2,
        "sketch_weight": 3,
        "style": "<watercolor>"
    }
}'
```

Fetch 异步函数调用

```js
const DASHSCOPE_API_KEY = 'YOUR_DASHSCOPE_API_KEY';

// 异步函数用于发送请求
async function sendRequest() {
  const url = 'https://dashscope.aliyuncs.com/api/v1/services/aigc/image2image/image-synthesis';
  const data = {
    "model": "wanx-sketch-to-image-lite",
    "input": {
        "sketch_image_url": "https://huarong123.oss-cn-hangzhou.aliyuncs.com/image/%E6%B6%82%E9%B8%A6%E8%8D%89%E5%9B%BE.png",
        "prompt": "一棵参天大树"
    },
    "parameters": {
        "size": "768*768",
        "n": 2,
        "sketch_weight": 3,
        "style": "<watercolor>"
    }
  };

  try {
    // 使用 fetch 发送 POST 请求
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'X-DashScope-Async': 'enable',
        'Authorization': `Bearer ${DASHSCOPE_API_KEY}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    // 检查响应状态
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    // 解析 JSON 响应
    const jsonResponse = await response.json();
    console.log(jsonResponse);
  } catch (error) {
    console.error('Error during fetch:', error);
  }
}

```
