# Axure RP8

---

## 产品原型图(prototype) 作用
产品研发时，描述产品设计的文档(UI样式，交互动作)

沟通需要的工具(领导，研发)

::: tip
原型图是产品思维，产品理念的重要传递手段
:::

## 原型图种类

- 线框图

用线段，色块，文字描述产品页面

用于早期讨论方案

![图 0](/storage/images/docs/learn/soft/axure/axure_20241018_104722.png)  

- 高保真图

还原产品运行效果 设计＋交互

用于客户/领导沟通

![图 1](/storage/images/docs/learn/soft/axure/axure_20241018_104733.png)  

- 简易需求文档PRD

页面中带有业务逻辑的描述和辅助说明

用于指导产品研发

![图 2](/storage/images/docs/learn/soft/axure/axure_20241018_104744.png)  

## 交互动效

![图 3](/storage/images/docs/learn/soft/axure/axure_20241018_104753.png)  

## 练习

---

![图 4](/storage/images/docs/learn/soft/axure/axure_20241018_104802.png)  

![图 5](/storage/images/docs/learn/soft/axure/axure_20241018_104810.png)  

![图 6](/storage/images/docs/learn/soft/axure/axure_20241018_151418.gif)  
