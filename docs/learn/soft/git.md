# Git

## 配置ssh key

- 查看是否有ssh key

```shell 
cd ~/.ssh
ls
```

- 生成ssh key

```shell 
ssh-keygen -t rsa -C "xxx@xxx.com"
```

- 获取ssh key公钥内容（id_rsa.pub）

```shell 
cd ~/.ssh
cat id_rsa.pub
```

- Github上新建公钥

![图 1](/storage/images/docs/learn/soft/git/git_20241031_172750.png)  

- 验证是否配置成功

```shell 
ssh -T git@github.com
```

## 新建仓库

- 配置Git,推送文件

```shell
echo "# TypeScript" >> README.md
git init
git add README.md
git commit -m "first commit"
git branch -M main
git remote add origin git@github.com:Zy-Tolearn/TypeScript.git
git push -u origin main
```