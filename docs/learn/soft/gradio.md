# Gradio学习记录

>Gradio 是一个开源 Python 包，可让您为机器学习模型、API 或任何任意 Python 函数快速构建演示或 Web 应用程序。然后，您可以使用 Gradio 的内置共享功能在几秒钟内共享指向您的演示或 Web 应用程序的链接。无需 JavaScript、CSS 或 Web 托管经验！

## Demo

```python{8,9,10}
import gradio as gr

def greet(name, intensity):
    return "Hello, " + name + "!" * int(intensity)

demo = gr.Interface(
    #传入创建UI的函数,输入组件,输出组件。
    fn=greet,
    inputs=["text", "slider"],
    outputs=["text"],
)

demo.launch()
```

