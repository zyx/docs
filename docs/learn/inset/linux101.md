# Linux 101

## Linux 起源

进入 GNU/Linux 世界，便意味着与 GNU 自由软件打交道。先看看一堆字母 g 开头的应用程序：

gcc: GNU 的 C 和 C++ 编译器
gdb: GNU 程序调试器
gzip: gz 格式压缩与解压缩工具
GNOME: 隶属于 GNU 项目的桌面环境
gimp: GNU 图像编辑工具

## Xubuntu安装