# 轮足机器人

## 参考项目

[无线充电式手机遥控FOC轮足巡检机器人](https://diy.szlcsc.com/p/drawing-board-squads/wireless-rechargeable-mobile-phone-remote-control-biped-wheel-inspection-robot)

【硬件】

主控：STM32C8T6 * 3 分别用来控制，驱动，电能传输；

通信方式：Uart,蓝牙

电源：充电芯片TP5100

其他：温湿度，陀螺仪



【结构】
