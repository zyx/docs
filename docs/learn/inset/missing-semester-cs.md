# The Missing Semester of Your CS Education

课程目的：挖掘工具潜力

[计算机教育中缺失的一课](https://missing-semester-cn.github.io/2020/course-shell/)

## Shell

指令是一段shell可以解释执行的代码，该指令不是编程关键字时会咨询`环境变量 $PATH`,包含接到某条指令后的程序搜索路径。

![图 1](/storage/images/docs/learn/inset/Linux/Linux_20241017_120913.png)  

## 目录操作

![图 2](/storage/images/docs/learn/inset/Linux/Linux_20241017_133947.png)  

文件和目录详细信息

![图 3](/storage/images/docs/learn/inset/Linux/Linux_20241017_134818.png)  

- `drwxr-xr-x`
  
d 表示这是一个目录（directory）。

rwx 表示所有者（owner）具有读（read）、写（write）和执行（execute）权限。

r-x 表示所属组（group）具有读和执行权限，但没有写权限。

r-x 表示其他用户（others）也具有读和执行权限，但没有写权限。

- `1`

表示有多少个硬链接指向这个目录。对于目录来说，这通常是 2，因为每个目录至少有一个 .（指向自身）和 ..（指向父目录）两个硬链接。这里的 1 可能是由于某些特殊情况或特定的文件系统设置。
- `missing`

所有者（owner）的用户名是 missing。
- `users`

所属组（group）的组名是 users。
- `4096`

目录的大小是 4096 字节。这是目录结构本身的大小，不是目录内容的大小。
- `Jun 15  2019`

目录的最后修改时间是 2019 年 6 月 15 日。
-  `missing`

目录的名称是 missing。

## 常用命令

`cd -` 到上一次位置

`cd ~` 到主目录

`mv` 重命名或移动

`cp` 拷贝

`rm -r` 递归删除

`rmdir` 删除空目录

`mkdir "My Photo"` 创建文件夹

`man ls` 查看ls的手册

## streams 流

`< `改变输入指向

`> `改变输出指向

`cat` 打印文件内容

`cat < hello.txt > hello2.txt`

`>>`追加

` | `将左边程序的输出作为右边程序的输入

`tail -n1` 打印最后一行

## 课后练习

