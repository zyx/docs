# JStraining 

## Backbone

>Model（模型）：代表应用程序的数据。模型可以监听数据的变化，并在数据发生变化时触发事件。

>Collection（集合）：包含模型的集合。集合可以提供丰富的功能，比如排序、过滤和查找模型。

>View（视图）：代表模型数据的可视化表示。视图可以监听模型的变化，并相应地更新 DOM。

>Router（路由器）：用于应用程序的路由管理。它可以根据 URL 变化来触发不同的函数，实现页面的无刷新更新。

路由规则加处理函数实现无刷新导航

`'': 'index'` 空路径时,触发index

`'show/:id': 'show'` 访问show/123时，触发函数show，123传入函数show,`:id`为动态段

`'download/*random': 'download'` `*random`是捕获所有剩余 URL 部分的动态段

`*other': 'default'` 捕获剩余不匹配的 URL 动态段

:::details 代码
```JS:line-numbers {1}
(function() {

window.App = {
	Models: {},
	Collections: {},
	Views: {},
	Router: {}
};

App.Router = Backbone.Router.extend({
	routes: {
		'': 'index',
		'show/:id': 'show',
		'download/*random': 'download',
		'search/:query': 'search',
		'*other': 'default'
	},

	index: function() {
		$(document.body).append("调用了 Index 路由<br>");
	},

	show: function(id) {
		$(document.body).append("调用了 Show 路由，id 等于 " + id + "<br>");
	},

	download: function(random) {
		$(document.body).append("调用了 Download 路由，参数等于 " + random + "<br>");
	},

	search: function(query) {
		$(document.body).append("调用了 Search 路由，参数等于 " + query + "<br>");
	},

	default: function(other) {
		$(document.body).append("你访问的 " + other + " 路由未定义<br>");
		
	}

});

new App.Router();
Backbone.history.start();

})();
```
:::

## Vue

双向绑定：HTML先创建DOM元素，v-model指令还未生效。Vue实例创建后挂载到指定DOM元素中，此时双向绑定并接管DOM元素

```HTML
<div id="journal">
  <input type="text" v-model="message">
  <div>{{message}}</div>
</div>
```

```JS
var journal = new Vue({
  el: '#journal',
  data: {
    message: 'Your first entry'
  }
});
```

## Rect

图形化界面函数化

### Mobx框架