# 一生一芯
---

## 项目概述
---

目标理念：让学生全流程的参与处理器芯片设计的环节。

学习流程：
- 阅读《项目概述》,填写报名问卷,开始预学习阶段。

> [!IMPORTANT]
> 每天填写学习记录。

能力提升：
> 技术水平：软硬件相结合的能力。
> 
> 科研能力：独立解决问题的能力、发掘研究方向的能力、探索未知领域的能力。
> 
> 心理素质：大家在学习的过程中会遇到各种各样的问题，伴随而来的可能还会有相当程度的挫败感和焦虑感，这些是学员们或多或少都会经历的。因此“一生一芯”计划在培养学生们的技术水平和科研能力之外，更重要的是教会大家如何用一种平常心去对待这些问题：当我们去做难度大且有意义的事情时，不好解决的问题必然是客观存在的，当然解决问题的方法也是一定存在的。既然问题和解决方法都始终存在，我们何必殚精竭虑地自怨自艾呢？取而代之的是，我们应该放平心态且对自己充满自信，努力从一次次失败中总结经验。通过几次正向反馈，大家不仅能收获满满的成就感，心理素质也会得到很好的锤炼。

## 预学习阶段
---

主要内容：
- 阅读如何提问"提问的智慧"和"别像弱智一样提问"，编写800字读后感。
- 填写《“一生一芯”通识问卷》
- 安装Linux系统。
- 搭建Verilator仿真环境。
- 完成Linux系统，数字电路，C语言习题。
- 完成PA1所有内容。

### 如何提问

提出激发思考的好问题 （了解技术细节而不是工具使用方法）
确保有解决问题的思路
提问前 从答案中学习 准备好你的问题
得到答案是学习的开始

使用Google搜索引擎搜索一般性问题
使用英文维基百科查阅概念
使用stack overflow问答网站搜索程序设计相关问题

STFW/RTFM/RTFSC

Search The F**king Web

## Linux入门

## 复习C语言

[Learn C the hard way](/learn/soft/the-hard-way-learn-c.md)

C++ 基础

## verilator仿真环境

verilog仿真工具 RTL功能仿真

![图 0](/storage/images/docs/learn/ysyx/ysyx_20241024_163443.png)  

**安装Verilator并运行示例**

https://github.com/verilator/verilator

https://verilator.org/guide/latest/install.html

https://www.veripool.org/ftp/verilator_doc.pdf

**Git Quick Install**

```shell
Installing Verilator from Git provides the most flexibility; 
for additional options and details, see Detailed Build Instructions below.

In brief, to install from git:

# Prerequisites:
#sudo apt-get install git help2man perl python3 make autoconf g++ flex bison ccache
#sudo apt-get install libgoogle-perftools-dev numactl perl-doc
#sudo apt-get install libfl2  # Ubuntu only (ignore if gives error)
#sudo apt-get install libfl-dev  # Ubuntu only (ignore if gives error)
#sudo apt-get install zlibc zlib1g zlib1g-dev  # Ubuntu only (ignore if gives error)

git clone https://github.com/verilator/verilator   # Only first time

# Every time you need to build:
unsetenv VERILATOR_ROOT  # For csh; ignore error if on bash
unset VERILATOR_ROOT  # For bash
cd verilator
git pull         # Make sure git repository is up-to-date
git tag          # See what versions exist
#git checkout master      # Use development branch (e.g. recent bug fixes)
#git checkout stable      # Use most recent stable release
#git checkout v{version}  # Switch to specified release version

autoconf         # Create ./configure script
./configure      # Configure and create Makefile
make -j `nproc`  # Build Verilator itself (if error, try just 'make')
sudo make install
```

**版本切换**

```shell
git pull
git tag
git checkout v5.008
autoconf
./configure
make -j `nproc`
sudo make install
```

## 视频课件

**计算机状态模型**

程序



## 相关链接

[提问的智慧](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way/blob/main/README-zh_CN.md)

[别像弱智一样提问](https://github.com/tangx/Stop-Ask-Questions-The-Stupid-Ways/blob/master/README.md)

[鸟哥的Linux私房菜](https://linux.vbird.org/linux_basic)

[GDB debugging tutorial for beginners](https://linuxconfig.org/gdb-debugging-tutorial-for-beginners)

[计算机教育中缺失的一课](https://missing-semester-cn.github.io/)

[Linux101](https://101.ustclug.org/)

[命令行的艺术](https://github.com/jlevy/the-art-of-command-line/blob/master/README-zh.md)

[笨方法学C](https://wizardforcel.gitbooks.io/lcthw/content/introduction.html)