# 标题

## 小标题

**深色**

`强调`

[GitHub](https://github.com/)

- 列表1
  
> 区块引用
>> 嵌套引用

分割线

---

::: tip 重要
记录容器
:::

::: warning 提醒
记录容器
:::

| 表头 | 表头 |
| ---- | ---- |
| 单元格 | 单元格 |
| 单元格 | 单元格 |

## 代码块

```shell
sudo apt-get update
```

// [!code highlight] 代码高亮

## 代码块中聚焦

```js
export default {
  data () {
    return {
      msg: 'Focused!' // [!code focus]
    }
  }
}
```

## 代码分组

::: code-group

```js [代码1.js]
/**
 * @type {import('vitepress').UserConfig}
 */
const config = {
  // ...
}

export default config
```

```ts [代码2.ts]
import type { UserConfig } from 'vitepress'

const config: UserConfig = {
  // ...
}

export default config
```

:::