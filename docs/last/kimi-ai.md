# Kimi AI 助手

# CSS 开发中的核心写法和最佳实践

先定义 body 全局样式，该选择器的优先级最低。

再用类选择器 .class 定义可复用的样式。

再用 ID 选择器 #id 定义唯一元素样式，该选择器优先级最高，用于需要精准控制的特定元素。

# 盒模型

每个 HTML 元素都是一个盒子，由四个同心层组成（从内到外）：

内容（Content） - 文字/图片等实际内容

内边距（Padding） - 内容与边框之间的缓冲空间

边框（Border） - 盒子的边界线

外边距（Margin） - 盒子与其他元素之间的间距

padding: 10px 20px 30px 40px; /* 上 右 下 左 */
padding: 10px 20px; /* 上下 10px，左右 20px */
padding: 10px; /* 四个方向都是 10px */

# Flex布局


# Bootstrap

# HTML

聊天框
    聊天容器
    用户信息样式
    AI信息样式
配置框
    Key输入框
功能按钮
    发送
    查询

# JavaScript

1. 事件监听

```JS
document.addEventListener('DOMContentLoaded', async () => {
  // 1. 获取页面元素
  const input = document.getElementById('myInput');
  const button = document.getElementById('myButton');

  // 2. 从本地存储加载配置
  const config = localStorage.getItem('config');

  // 3. 绑定事件
  button.addEventListener('click', async () => {
    const data = await fetchData();
    updateUI(data);
  });

  // 4. 初始化异步操作
  const initialData = await loadInitialData();
  renderData(initialData);
});
```

2. 配置面板弹出框


```JS
const panel = document.querySelector('.config-panel');
const configButton = document.getElementById('config-button');

// 点击配置按钮时切换面板
configButton.addEventListener('click', (e) => {
  e.stopPropagation(); // 阻止事件冒泡到父元素
  panel.classList.toggle('show'); // 显示或隐藏面板
});

// 点击关闭按钮时隐藏面板
document.querySelector('.close-btn').addEventListener('click', () => {
  panel.classList.remove('show'); // 强制移除 show 类
});

// 点击页面其他区域时隐藏面板
document.addEventListener('click', (e) => {
  const isClickInsidePanel = panel.contains(e.target); // 点击是否在面板内
  const isClickOnConfigButton = e.target === configButton || configButton.contains(e.target); // 点击的是配置按钮或其子元素

  if (!isClickInsidePanel && !isClickOnConfigButton) {
    panel.classList.remove('show'); // 隐藏面板
  }
});
```

3. 流式传输

发送带有stream: true的POST请求。
使用response.body.getReader()读取流式数据。
逐块解析数据并提取有效内容。
实时调用displayMessage显示内容。
可通过stopStreaming函数终止流式传输。

4. 信息显示

根据内容类型，创建并设置`<div>`元素的对齐方式。
将内容插入到`<div>`中，并替换换行符。
将`<div>`追加到chat-container。
滚动chat-container到最底部。